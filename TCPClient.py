import socket
import ctypes

BUFFER_SIZE = 65535

class TCPClient:
    def __init__(self, address, port):
        self.fPackerNumber = 0
        self.fClientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.fClientSocket.connect((address, port))
        
    def __del__(self):
        self.fClientSocket.close()

    def makeFourByteArray(self, integer):
        integerByte0 = ctypes.c_ubyte((integer >>  0) & 0xff)
        integerByte1 = ctypes.c_ubyte((integer >>  8) & 0xff)
        integerByte2 = ctypes.c_ubyte((integer >> 16) & 0xff)
        integerByte3 = ctypes.c_ubyte((integer >> 24) & 0xff)
        integerByte = bytearray(integerByte3) + bytearray(integerByte2) + bytearray(integerByte1) + bytearray(integerByte0)
        return integerByte

    def encodePacket(self, message, packetNumber):
        fullMessage = self.makeFourByteArray(len(message)+8) + self.makeFourByteArray(packetNumber) + bytearray(message, "utf-8")
        return fullMessage;
    
    def decodePacket(self, message):
        return message[8:].decode();

    def sendAndReceivePacket(self, message):
        self.fClientSocket.send(self.encodePacket(message, self.fPackerNumber))
        self.fPackerNumber = self.fPackerNumber +1
        data = self.fClientSocket.recv(BUFFER_SIZE)
        return self.decodePacket(data)

# Usage
# client = TCPClient('127.0.0.1', 7000)
# reply1 = client.sendAndReceivePacket("GetStatus")

