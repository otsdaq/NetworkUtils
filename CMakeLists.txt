if(NOT DEFINED ENV{OTSDAQ_CMSTRACKER_DIR})

    MESSAGE(STATUS " ")
    MESSAGE(STATUS "    ${BoldBlue}========================================================================================================${Reset}")
    MESSAGE(STATUS "    ${BoldYellow}NETWORK UTILS${Reset} [stand-alone]: [${BoldCyan}NetworkUtils/CMakeLists.txt${Reset}]. ${BoldRed}Begin...${Reset}")
    MESSAGE(STATUS " ")

    #includes
    include_directories(${PROJECT_SOURCE_DIR})

    # Boost also needs to be linked
    include_directories(${Boost_INCLUDE_DIRS})
    link_directories(${Boost_LIBRARY_DIRS})
    set(LIBS ${LIBS} ${Boost_REGEX_LIBRARY})

    #find source files
    file(GLOB SOURCES *.cc)
    add_library(NetworkUtils STATIC ${SOURCES})


    message("--     ${BoldCyan}#### End ####${Reset}")

    MESSAGE(STATUS " ")
    MESSAGE(STATUS "    ${BoldYellow}NETWORK UTILS${Reset} [stand-alone]: [${BoldCyan}NetworkUtils/CMakeLists.txt${Reset}]. ${BoldGreen}DONE!${Reset}") 
    MESSAGE(STATUS "    ${BoldBlue}========================================================================================================${Reset}") 
    MESSAGE(STATUS " ")

else()

    MESSAGE(STATUS " ")
    MESSAGE(STATUS "    ${BoldBlue}========================================================================================================${Reset}")
    MESSAGE(STATUS "    ${BoldYellow}NETWORK UTILS${Reset} [otsdaq]: [${BoldCyan}NetworkUtils/CMakeLists.txt${Reset}]. ${BoldRed}Begin...${Reset}")
    MESSAGE(STATUS " ")
            
    cet_make(LIBRARY_NAME NetworkUtils
            LIBRARIES
    )

    install_headers()
    install_source()
    
    MESSAGE(STATUS " ")
    MESSAGE(STATUS "    ${BoldYellow}NETWORK UTILS${Reset} [otsdaq]: [${BoldCyan}NetworkUtils/CMakeLists.txt${Reset}]. ${BoldGreen}DONE!${Reset}") 
    MESSAGE(STATUS "    ${BoldBlue}========================================================================================================${Reset}") 
    MESSAGE(STATUS " ")

endif()


